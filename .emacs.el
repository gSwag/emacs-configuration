;;; emacs.el --- Emacs init file
;;; Commentary:
;; Emacs configuration
;; Custom configuration for Emacs on different platforms.  May
;; require one or more dependencies to make everything work smoothly
;; on all platforms.  Platforms currently include the following -
;;
;; 1. Linux
;; 2. Windows
;;
;; Author: Asif Mahmud Shimon
;; Date: 8 Oct 2020
;;
;; Requirements:
;; 1. JetBrains Mono Font;;
;; 2. Run - all-the-icons-install to download/install
;;    required icon fonts.  In case of download, you have to install
;;    fonts manually.
;;
;; Note: To support more platforms, tests must be done exclusively.
;;

;;; Code:

;;; Disable built-in package manager
(setq package-enable-at-startup nil)

;;; Start up with debug option
(setq toggle-debug-on-error t)

;;; Enable straight package manager
(setq-default straight-cache-autoloads t
              straight-vc-git-default-clone-depth 1)
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;; Load config file without compiling
(setq-default org-startup-indented t
              org-src-tab-acts-natively t
              org-hide-emphasis-markers t
              org-fontify-done-headline t
              org-hide-leading-stars t
              org-pretty-entities t
              org-odd-levels-only t
              org-indent-mode-turns-on-hiding-stars nil)

(straight-use-package 'use-package)
(use-package org
  :straight (:type built-in))
(org-babel-load-file "~/.emacs-config.org")
